package base

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type RequiredFieldTestCase[T any] interface {
	// ZeroValueStruct produces the struct, with all fields set to their zero value. Must have a required field called `ID`, which marshals to `id`
	ZeroValueStruct() T

	// UnspecifiedFieldStruct produces the struct, with the `ID` field explicitly set to the underlying type's "unspecified" configuration
	UnspecifiedFieldStruct() T

	// NullFieldStruct produces the struct, with the `ID` field explicitly set as if the field was sent as `null`
	NullFieldStruct() T

	// FieldWithValueStruct produces the struct, with the `ID` field
	FieldWithValueStruct(v int) T

	// IsSpecified indicates whether the `ID` field was provided in the request
	IsSpecified(t T) bool

	// IsNull indicates whether the `ID` field's underlying type indicates that it was explicitly set as `null`
	IsNull(t T) bool

	// IsValue indicates whether the `ID` field's underlying type indicates that it was explicitly set as the given `expected` value
	IsValue(t T, expected int) bool
}

func RequiredFieldTester[T any](t *testing.T, tc RequiredFieldTestCase[T]) {
	t.Run("MarshalJSON", func(t *testing.T) {
		t.Run("when the field isn't set (by default)", func(t *testing.T) {
			obj := tc.ZeroValueStruct()

			b, err := json.Marshal(obj)
			require.NoError(t, err)

			assert.Equal(t, `{"id":0}`, string(b))
		})

		t.Run("when the field isn't set (explicitly)", func(t *testing.T) {
			obj := tc.UnspecifiedFieldStruct()

			b, err := json.Marshal(obj)
			require.NoError(t, err)

			assert.Equal(t, `{"id":0}`, string(b))
		})

		t.Run("when the field is explicitly set to null", func(t *testing.T) {
			obj := tc.NullFieldStruct()

			b, err := json.Marshal(obj)
			require.NoError(t, err)

			assert.Equal(t, `{"id":null}`, string(b))
		})

		t.Run("when the field has a value explicitly set to the zero value", func(t *testing.T) {
			var i int
			obj := tc.FieldWithValueStruct(i)

			b, err := json.Marshal(obj)
			require.NoError(t, err)

			assert.Equal(t, `{"id":0}`, string(b))
		})

		t.Run("when the field has a value explicitly set to a value", func(t *testing.T) {
			i := 12345
			obj := tc.FieldWithValueStruct(i)

			b, err := json.Marshal(obj)
			require.NoError(t, err)

			assert.Equal(t, `{"id":12345}`, string(b))
		})
	})

	t.Run("UnmarshalJSON", func(t *testing.T) {
		t.Run("when the field isn't set", func(t *testing.T) {
			obj := tc.ZeroValueStruct()
			err := json.Unmarshal([]byte(`
		{
		}
		`), &obj)
			require.NoError(t, err)

			assert.False(t, tc.IsSpecified(obj))
			assert.False(t, tc.IsNull(obj))
		})

		t.Run("when the field is explicitly set to null", func(t *testing.T) {
			obj := tc.ZeroValueStruct()
			err := json.Unmarshal([]byte(`
		{
			"id": null
		}
		`), &obj)
			require.NoError(t, err)

			assert.True(t, tc.IsSpecified(obj))
			assert.True(t, tc.IsNull(obj))
		})

		t.Run("when the field has a value explicitly set to the zero value", func(t *testing.T) {
			obj := tc.ZeroValueStruct()
			err := json.Unmarshal([]byte(`
		{
			"id": 0
		}
		`), &obj)
			require.NoError(t, err)

			assert.True(t, tc.IsSpecified(obj))
			assert.False(t, tc.IsNull(obj))
			assert.True(t, tc.IsValue(obj, 0))
		})

		t.Run("when the field has a value explicitly set to a value", func(t *testing.T) {
			obj := tc.ZeroValueStruct()
			err := json.Unmarshal([]byte(`
		{
			"id": 55555
		}
		`), &obj)
			require.NoError(t, err)

			assert.True(t, tc.IsSpecified(obj))
			assert.False(t, tc.IsNull(obj))
			assert.True(t, tc.IsValue(obj, 55555))
		})
	})
}

type OptionalFieldTestCase[T any] interface {
	// ZeroValueStruct produces the struct, with all fields set to their zero value. Must have an optional field called `ID`, which marshals to `id`
	ZeroValueStruct() T

	// UnspecifiedFieldStruct produces the struct, with the `ID` field explicitly set to the underlying type's "unspecified" configuration
	UnspecifiedFieldStruct() T

	// NullFieldStruct produces the struct, with the `ID` field explicitly set as if the field was sent as `null`
	NullFieldStruct() T

	// FieldWithValueStruct produces the struct, with the `ID` field
	FieldWithValueStruct(v string) T

	// IsSpecified indicates whether the `ID` field was provided in the request
	IsSpecified(t T) bool

	// IsNull indicates whether the `ID` field's underlying type indicates that it was explicitly set as `null`
	IsNull(t T) bool

	// IsValue indicates whether the `ID` field's underlying type indicates that it was explicitly set as the given `expected` value
	IsValue(t T, expected string) bool
}

func OptionalFieldTester[T any](t *testing.T, tc OptionalFieldTestCase[T]) {
	t.Run("MarshalJSON", func(t *testing.T) {
		t.Run("when the field isn't set (by default)", func(t *testing.T) {
			obj := tc.ZeroValueStruct()

			b, err := json.Marshal(obj)
			require.NoError(t, err)

			assert.Equal(t, `{}`, string(b))
		})

		t.Run("when the field isn't set (explicitly)", func(t *testing.T) {
			obj := tc.UnspecifiedFieldStruct()

			b, err := json.Marshal(obj)
			require.NoError(t, err)

			assert.Equal(t, `{}`, string(b))
		})

		t.Run("when the field is explicitly set to null", func(t *testing.T) {
			obj := tc.NullFieldStruct()

			b, err := json.Marshal(obj)
			require.NoError(t, err)

			assert.Equal(t, `{"id":null}`, string(b))
		})

		t.Run("when the field has a value explicitly set to the zero value", func(t *testing.T) {
			var v string
			obj := tc.FieldWithValueStruct(v)

			b, err := json.Marshal(obj)
			require.NoError(t, err)

			assert.Equal(t, `{"id":""}`, string(b))
		})

		t.Run("when the field has a value explicitly set to a value", func(t *testing.T) {
			v := "foo"
			obj := tc.FieldWithValueStruct(v)

			b, err := json.Marshal(obj)
			require.NoError(t, err)

			assert.Equal(t, `{"id":"foo"}`, string(b))
		})
	})

	t.Run("UnmarshalJSON", func(t *testing.T) {
		t.Run("when the field isn't set", func(t *testing.T) {
			obj := tc.ZeroValueStruct()
			err := json.Unmarshal([]byte(`
		{
		}
		`), &obj)
			require.NoError(t, err)

			assert.False(t, tc.IsSpecified(obj))
			assert.False(t, tc.IsNull(obj))
		})

		t.Run("when the field is explicitly set to null", func(t *testing.T) {
			obj := tc.ZeroValueStruct()
			err := json.Unmarshal([]byte(`
		{
			"id": null
		}
		`), &obj)
			require.NoError(t, err)

			assert.True(t, tc.IsSpecified(obj))
			assert.True(t, tc.IsNull(obj))
		})

		t.Run("when the field has a value explicitly set to the zero value", func(t *testing.T) {
			obj := tc.ZeroValueStruct()
			err := json.Unmarshal([]byte(`
		{
			"id": ""
		}
		`), &obj)
			require.NoError(t, err)

			assert.True(t, tc.IsSpecified(obj))
			assert.False(t, tc.IsNull(obj))
			assert.True(t, tc.IsValue(obj, ""))
		})

		t.Run("when the field has a value explicitly set to a value", func(t *testing.T) {
			obj := tc.ZeroValueStruct()
			err := json.Unmarshal([]byte(`
		{
			"id": "foo"
		}
		`), &obj)
			require.NoError(t, err)

			assert.True(t, tc.IsSpecified(obj))
			assert.False(t, tc.IsNull(obj))
			assert.True(t, tc.IsValue(obj, "foo"))
		})
	})
}
