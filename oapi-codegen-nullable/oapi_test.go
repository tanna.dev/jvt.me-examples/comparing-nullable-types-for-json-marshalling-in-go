package oapi_test

import (
	"testing"

	"github.com/oapi-codegen/nullable"
	base "gitlab.com/tanna.dev/jvt.me-examples/comparing-nullable-types-for-json-marshalling-in-go"
)

type RequiredInput struct {
	ID nullable.Nullable[int] `json:"id"`
}

type requiredField struct{}

// ZeroValueStruct produces the struct, with all fields set to their zero value. Must have a required field called `ID`, which marshals to `id`
func (requiredField) ZeroValueStruct() RequiredInput {
	return RequiredInput{}
}

// UnspecifiedFieldStruct produces the struct, with the `ID` field explicitly set to the underlying type's "unspecified" configuration
func (requiredField) UnspecifiedFieldStruct() RequiredInput {
	inp := RequiredInput{}
	inp.ID.SetUnspecified()
	return inp
}

// NullFieldStruct produces the struct, with the `ID` field explicitly set as if the field was sent as `null`
func (requiredField) NullFieldStruct() RequiredInput {
	inp := RequiredInput{}
	inp.ID.SetNull()
	return inp
}

// FieldWithValueStruct produces the struct, with the `ID` field
func (requiredField) FieldWithValueStruct(v int) RequiredInput {
	inp := RequiredInput{}
	inp.ID.Set(v)

	return inp
}

// IsSpecified indicates whether the `ID` field was provided in the request
func (requiredField) IsSpecified(t RequiredInput) bool {
	return t.ID.IsSpecified()
}

// IsNull indicates whether the `ID` field's underlying type indicates that it was explicitly set as `null`
func (requiredField) IsNull(t RequiredInput) bool {
	return t.ID.IsNull()
}

// IsValue indicates whether the `ID` field's underlying type indicates that it was explicitly set as the given `expected` value
func (requiredField) IsValue(t RequiredInput, expected int) bool {
	val, err := t.ID.Get()
	if err != nil {
		return false
	}
	return val == expected
}

type OptionalInput struct {
	ID nullable.Nullable[string] `json:"id,omitempty"`
}

type optionalField struct{}

// ZeroValueStruct produces the struct, with all fields set to their zero value. Must have a optional field called `ID`, which marshals to `id`
func (optionalField) ZeroValueStruct() OptionalInput {
	return OptionalInput{}
}

// UnspecifiedFieldStruct produces the struct, with the `ID` field explicitly set to the underlying type's "unspecified" configuration
func (optionalField) UnspecifiedFieldStruct() OptionalInput {
	inp := OptionalInput{}
	inp.ID.SetUnspecified()
	return inp
}

// NullFieldStruct produces the struct, with the `ID` field explicitly set as if the field was sent as `null`
func (optionalField) NullFieldStruct() OptionalInput {
	inp := OptionalInput{}
	inp.ID.SetNull()
	return inp
}

// FieldWithValueStruct produces the struct, with the `ID` field
func (optionalField) FieldWithValueStruct(v string) OptionalInput {
	inp := OptionalInput{}
	inp.ID.Set(v)

	return inp
}

// IsSpecified indicates whether the `ID` field was provided in the request
func (optionalField) IsSpecified(t OptionalInput) bool {
	return t.ID.IsSpecified()
}

// IsNull indicates whether the `ID` field's underlying type indicates that it was explicitly set as `null`
func (optionalField) IsNull(t OptionalInput) bool {
	return t.ID.IsNull()
}

// IsValue indicates whether the `ID` field's underlying type indicates that it was explicitly set as the given `expected` value
func (optionalField) IsValue(t OptionalInput, expected string) bool {
	val, err := t.ID.Get()
	if err != nil {
		return false
	}
	return val == expected
}

func TestOapiCodegenNullable(t *testing.T) {
	t.Run("with a required field", func(t *testing.T) {
		tc := requiredField{}

		base.RequiredFieldTester(t, tc)
	})

	t.Run("with an optional field", func(t *testing.T) {
		tc := optionalField{}

		base.OptionalFieldTester(t, tc)
	})
}
