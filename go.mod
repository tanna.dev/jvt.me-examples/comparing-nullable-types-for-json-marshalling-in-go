module gitlab.com/tanna.dev/jvt.me-examples/comparing-nullable-types-for-json-marshalling-in-go

go 1.21.5

require (
	github.com/go-faster/jx v1.1.0
	github.com/oapi-codegen/nullable v1.0.1
	github.com/stretchr/testify v1.8.4
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-faster/errors v0.6.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/segmentio/asm v1.2.0 // indirect
	golang.org/x/sys v0.1.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
