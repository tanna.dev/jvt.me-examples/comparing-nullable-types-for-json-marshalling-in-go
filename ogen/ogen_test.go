package ogen

import (
	"testing"

	base "gitlab.com/tanna.dev/jvt.me-examples/comparing-nullable-types-for-json-marshalling-in-go"
)

type RequiredInput struct {
	ID NilInt `json:"id"`
}

type requiredField struct{}

// ZeroValueStruct produces the struct, with all fields set to their zero value. Must have a required field called `ID`, which marshals to `id`
func (requiredField) ZeroValueStruct() RequiredInput {
	return RequiredInput{}
}

// UnspecifiedFieldStruct produces the struct, with the `ID` field explicitly set to the underlying type's "unspecified" configuration
func (requiredField) UnspecifiedFieldStruct() RequiredInput {
	inp := RequiredInput{}
	// inp.ID.Sent = false // TODO
	return inp
}

// NullFieldStruct produces the struct, with the `ID` field explicitly set as if the field was sent as `null`
func (requiredField) NullFieldStruct() RequiredInput {
	inp := RequiredInput{}
	inp.ID.Null = true
	return inp
}

// FieldWithValueStruct produces the struct, with the `ID` field
func (requiredField) FieldWithValueStruct(v int) RequiredInput {
	inp := RequiredInput{}
	inp.ID.Value = v

	return inp
}

// IsSpecified indicates whether the `ID` field was provided in the request
func (requiredField) IsSpecified(t RequiredInput) bool {
	return true // TODO
}

// IsNull indicates whether the `ID` field's underlying type indicates that it was explicitly set as `null`
func (requiredField) IsNull(t RequiredInput) bool {
	return t.ID.Null == true
}

// IsValue indicates whether the `ID` field's underlying type indicates that it was explicitly set as the given `expected` value
func (requiredField) IsValue(t RequiredInput, expected int) bool {
	return t.ID.Value == expected
}

type OptionalInput struct {
	ID OptNilString `json:"id"`
}

type optionalField struct{}

// ZeroValueStruct produces the struct, with all fields set to their zero value. Must have a optional field called `ID`, which marshals to `id`
func (optionalField) ZeroValueStruct() OptionalInput {
	return OptionalInput{}
}

// UnspecifiedFieldStruct produces the struct, with the `ID` field explicitly set to the underlying type's "unspecified" configuration
func (optionalField) UnspecifiedFieldStruct() OptionalInput {
	inp := OptionalInput{}
	inp.ID.Set = true
	return inp
}

// NullFieldStruct produces the struct, with the `ID` field explicitly set as if the field was sent as `null`
func (optionalField) NullFieldStruct() OptionalInput {
	inp := OptionalInput{}
	inp.ID.Null = true
	return inp
}

// FieldWithValueStruct produces the struct, with the `ID` field
func (optionalField) FieldWithValueStruct(v string) OptionalInput {
	inp := OptionalInput{}
	inp.ID.Value = v

	return inp
}

// IsSpecified indicates whether the `ID` field was provided in the request
func (optionalField) IsSpecified(t OptionalInput) bool {
	return t.ID.Set == true
}

// IsNull indicates whether the `ID` field's underlying type indicates that it was explicitly set as `null`
func (optionalField) IsNull(t OptionalInput) bool {
	return t.ID.Set == true && t.ID.Null == true
}

// IsValue indicates whether the `ID` field's underlying type indicates that it was explicitly set as the given `expected` value
func (optionalField) IsValue(t OptionalInput, expected string) bool {
	return t.ID.Value == expected
}

func TestOgen(t *testing.T) {
	t.Run("with a required field", func(t *testing.T) {
		tc := requiredField{}

		base.RequiredFieldTester(t, tc)
	})

	t.Run("with an optional field", func(t *testing.T) {
		tc := optionalField{}

		base.OptionalFieldTester(t, tc)
	})
}
