# Comparing nullable types for JSON marshalling in Go

Example code to go alongside [How do you represent a JSON field in Go that could be absent, null or have a value?](https://www.jvt.me/posts/2024/01/09/go-json-nullable/)'s investigations into nullable types in Go, with a test harness to make it easier to validate.

Check out the latest GitLab CI run for stats.

Licensed: Apache-2.0
