package huma_test

import (
	"bytes"
	"encoding/json"
	"testing"

	base "gitlab.com/tanna.dev/jvt.me-examples/comparing-nullable-types-for-json-marshalling-in-go"
)

// via https://github.com/danielgtaylor/huma/blob/v2.2.0/examples/omit/main.go

// OmittableNullable is a field which can be omitted from the input,
// set to `null`, or set to a value. Each state is tracked and can
// be checked for in handling code.
type OmittableNullable[T any] struct {
	Sent  bool
	Null  bool
	Value T
}

// UnmarshalJSON unmarshals this value from JSON input.
func (o *OmittableNullable[T]) UnmarshalJSON(b []byte) error {
	if len(b) > 0 {
		o.Sent = true
		if bytes.Equal(b, []byte("null")) {
			o.Null = true
			return nil
		}
		return json.Unmarshal(b, &o.Value)
	}
	return nil
}

type RequiredInput struct {
	ID OmittableNullable[int] `json:"id"`
}

type requiredField struct{}

// ZeroValueStruct produces the struct, with all fields set to their zero value. Must have a required field called `ID`, which marshals to `id`
func (requiredField) ZeroValueStruct() RequiredInput {
	return RequiredInput{}
}

// UnspecifiedFieldStruct produces the struct, with the `ID` field explicitly set to the underlying type's "unspecified" configuration
func (requiredField) UnspecifiedFieldStruct() RequiredInput {
	inp := RequiredInput{}
	inp.ID.Sent = false
	return inp
}

// NullFieldStruct produces the struct, with the `ID` field explicitly set as if the field was sent as `null`
func (requiredField) NullFieldStruct() RequiredInput {
	inp := RequiredInput{}
	inp.ID.Null = true
	return inp
}

// FieldWithValueStruct produces the struct, with the `ID` field
func (requiredField) FieldWithValueStruct(v int) RequiredInput {
	inp := RequiredInput{}
	inp.ID.Value = v

	return inp
}

// IsSpecified indicates whether the `ID` field was provided in the request
func (requiredField) IsSpecified(t RequiredInput) bool {
	return t.ID.Sent == true
}

// IsNull indicates whether the `ID` field's underlying type indicates that it was explicitly set as `null`
func (requiredField) IsNull(t RequiredInput) bool {
	return t.ID.Sent == true && t.ID.Null == true
}

// IsValue indicates whether the `ID` field's underlying type indicates that it was explicitly set as the given `expected` value
func (requiredField) IsValue(t RequiredInput, expected int) bool {
	return t.ID.Value == expected
}

type OptionalInput struct {
	ID OmittableNullable[string] `json:"id,omitempty"`
}

type optionalField struct{}

// ZeroValueStruct produces the struct, with all fields set to their zero value. Must have a optional field called `ID`, which marshals to `id`
func (optionalField) ZeroValueStruct() OptionalInput {
	return OptionalInput{}
}

// UnspecifiedFieldStruct produces the struct, with the `ID` field explicitly set to the underlying type's "unspecified" configuration
func (optionalField) UnspecifiedFieldStruct() OptionalInput {
	inp := OptionalInput{}
	inp.ID.Sent = false
	return inp
}

// NullFieldStruct produces the struct, with the `ID` field explicitly set as if the field was sent as `null`
func (optionalField) NullFieldStruct() OptionalInput {
	inp := OptionalInput{}
	inp.ID.Null = true
	return inp
}

// FieldWithValueStruct produces the struct, with the `ID` field
func (optionalField) FieldWithValueStruct(v string) OptionalInput {
	inp := OptionalInput{}
	inp.ID.Value = v

	return inp
}

// IsSpecified indicates whether the `ID` field was provided in the request
func (optionalField) IsSpecified(t OptionalInput) bool {
	return t.ID.Sent == true
}

// IsNull indicates whether the `ID` field's underlying type indicates that it was explicitly set as `null`
func (optionalField) IsNull(t OptionalInput) bool {
	return t.ID.Sent == true && t.ID.Null == true
}

// IsValue indicates whether the `ID` field's underlying type indicates that it was explicitly set as the given `expected` value
func (optionalField) IsValue(t OptionalInput, expected string) bool {
	return t.ID.Value == expected
}

func TestHuma(t *testing.T) {
	t.Run("with a required field", func(t *testing.T) {
		tc := requiredField{}

		base.RequiredFieldTester(t, tc)
	})

	t.Run("with an optional field", func(t *testing.T) {
		tc := optionalField{}

		base.OptionalFieldTester(t, tc)
	})
}
